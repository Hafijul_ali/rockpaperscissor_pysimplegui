import PySimpleGUI as gui
from random import choice, randint
from time import sleep

ROCK_IMG = "./images/Rock.png"
PAPER_IMG = "./images/Paper.png"
SCISSORS_IMG = "./images/Scissors.png"
BUTTON_WIDTH = BUTTON_LENGTH = 10
FONT_SIZE = 7
FONT = "AgencyFB"
WIN_SIZE = WINDOW_WIDTH, WINDOW_HEIGHT = 600, 400
score = 0  # stores the score of the user as game progresses
gui.theme("Material 2")

window_layout = [
    [gui.Text("Stone-Paper-Scissor", size=(WINDOW_WIDTH, 1), font=(
        "Comic Sans", 20), justification="center")],
    [gui.Text("Score : ", size=(WINDOW_WIDTH//2, 1),
              key="_scoreboard_", font=(
        "Comic Sans", 16), justification="left")],
    [gui.Button(" ", image_filename=ROCK_IMG, size=(BUTTON_WIDTH, BUTTON_LENGTH), key="Rock"), gui.Button(
        " ", image_filename=PAPER_IMG, size=(BUTTON_WIDTH, BUTTON_LENGTH), key="Paper"), gui.Button(" ", image_filename=SCISSORS_IMG, size=(BUTTON_WIDTH, BUTTON_LENGTH), key="Scissors")],
    [gui.Text("_"*40)],
    [gui.Text(" ", size=(WINDOW_WIDTH, 3),
              key="_gameOutput_", font=("Comic Sans", 15), justification="center")]
]


def update_scoreboard(score):
    window["_scoreboard_"].update("Score :{0}".format(score))


def display_result(user_move, computer_move, result):
    window["_gameOutput_"].update("User's Move :{0}\nComputer's Move :{1}\nResult :{2}".format(
        user_move, computer_move, result))


def get_winner(user_move, computer_move):
    global score
    if user_move == computer_move:
        result = "Its a Draw"
    elif user_move == "Paper" and computer_move == "Scissors":
        score -= 10
        result = "Computer wins"
    elif user_move == "Rock" and computer_move == "Paper":
        score -= 10
        result = "Computer wins"
    elif user_move == "Scissors" and computer_move == "Rock":
        score -= 10
        result = "Computer wins"
    elif user_move == "Paper" and computer_move == "Rock":
        score += 10
        result = "User wins"
    elif user_move == "Scissors" and computer_move == "Paper":
        score += 10
        result = "User wins"
    elif user_move == "Rock" and computer_move == "Scissors":
        score += 10
        result = "User wins"
    return result


window = gui.Window("Stone-Paper-Scissors", window_layout,
                    size=(WINDOW_WIDTH, WINDOW_HEIGHT), element_justification="center")

while True:

    event, values = window.read()

    if event in (gui.WIN_CLOSED, "Exit"):
        break

    if event in ("Rock", "Paper", "Scissors"):
        computer_move = choice(["Rock", "Paper", "Scissors"])
        result = get_winner(event, computer_move)
        display_result(event, computer_move, result)
        update_scoreboard(score)
